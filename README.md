# README #

Khemlabs - Kickstarter - ldap

### What is this repository for?

This lib is intended for developers that are using the khemlabs kickstarter framework, it
is a rapper of the ldapjs lib

### Requirements

> Khemlabs kickstarter server

### methods

#### auth(ldapConfig, username, password)

This method tries to bind a ldap user with a password, the libs creates the user on mondodb

##### ldapConfig

```nodejs
{
	user: 'the dn of the readonly user',
	password: 'the password of the readonly user',
	search_base: 'dn search base string',
	url: 'the server url'
}
```

### TODO

ONLY WORKS WITH MONGODB, update it to work with POSTGRESQL

### Who do I talk to?

* dnangelus repo owner and admin
* developer elgambet and khemlabs