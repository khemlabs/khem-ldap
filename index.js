const ldap = require('ldapjs');
const odm = require('khem-mongoose');
const shortid = require('shortid');

const bindNewClient = (url, user, password) => {
  const cl = ldap.createClient({ url });
  return new Promise((resolve, reject) => {
    cl.bind(user, password, err => {
      if (err) return reject(err);
      return resolve(cl);
    });
  });
};

const getDNFromUsername = (base, client, username) => {
  return new Promise((resolve, reject) => {
    // Search CN of user
    const opts = { filter: `CN=${username}`, scope: 'sub' };
    client.search(base, opts, (err, res) => {
      if (err) return reject(err);
      let dn;
      res.on('searchEntry', function(entry) {
        dn = entry.objectName;
      });
      res.on('end', function() {
        if (!dn) return reject('user not found');
        client.unbind();
        return resolve(dn);
      });
    });
  });
};

const _auth = (conf, username, password) => {
  let dn = {};
  return bindNewClient(conf.url, conf.user, conf.password)
    .then(client => getDNFromUsername(conf.search_base, client, username))
    .then(dndata => {
      dn = dndata;
      return bindNewClient(conf.url, dn, password);
    })
    .then(userLogged => {
      userLogged.unbind();
      return Promise.resolve({
        username: dn.substring(3, dn.indexOf(',')),
        hash: dn.substring(dn.indexOf(',') + 1)
      });
    });
};

const findOrCreateUser = dn => {
  const data = {
    where: { username: dn.username, type: 'sso' },
    defaults: {
      username: dn.username,
      first_name: dn.username,
      last_name: dn.username,
      password: shortid.generate(),
      sso: {
        type: 'active_directory',
        hash: dn.hash
      },
      status: 'isactive',
      type: 'sso',
      createdBy: 'system'
    }
  };
  return odm.models.User.findOrCreate(data);
};

/**
 * Tries to bind an ldap connection
 * @param ldapConfig {object} readonly { \
 * 		user: 'the dn of the readonly user', \
 * 		password: 'the password of the readonly user', \
 * 		search_base: 'dn search base string', \
 * 		url: 'the server url' \
 * 	}
 * @param usenrame {string} 
 * @param password {string}
 */
exports.auth = (ldapConfig, username, password) => {
  let dn = {};
  return _auth(ldapConfig, username, password)
    .then(dndata => {
      dn = dndata;
      // Create user on DB, with username = first_name = last_name and type (user?, medic?)
      // and without password and return it.
      const data = { where: { name: dn.hash }, defaults: { name: dn.hash, role: 'user' } };
      return odm.models.Sso.findOrCreate(data);
    })
    .then(sso => findOrCreateUser(dn));
};
